const { Router } = require("express");
const router = Router();

var controllerPacientes= require('../src/controllers/controllerPacientes');
var controllerMedicos= require('../src/controllers/controllerMedicos');
var controllerCitas= require('../src/controllers/controllerCitas');

router.get('/prueba',controllerPacientes.prueba);

router.post('/crearpaciente',controllerPacientes.savePacientes);
router.post('/crearmedico',controllerMedicos.saveMedicos);
router.post('/crearcita',controllerCitas.saveCitas);

router.get('/buscarcita/:id',controllerCitas.buscarCita);
router.get('/buscarmedico/:id',controllerMedicos.buscarMedico);
router.get('/buscarpaciente/:id',controllerPacientes.buscarPaciente);

router.put('/actualizarcita/:id',controllerCitas.updateCita);
router.put('/actualizarmedico/:id',controllerMedicos.updateMedico);
router.put('/actualizarpaciente/:id',controllerPacientes.updatePaciente);


router.delete('/eliminarcita/:id',controllerCitas.deleteCita);
router.delete('/eliminarmedico/:id',controllerMedicos.deleteMedico);
router.delete('/eliminarpaciente/:id',controllerPacientes.deletePaciente);



module.exports=router;

