var mongoose =require('mongoose');
var schema = mongoose.Schema;
var pacientesShema = schema({

    nombre: String,
    apellido: String,
    documento_identidad: Number,
    ciudad: String,
    direccion: String,
    telefono: Number,
    email: String
   
   });

const Pacientes = mongoose.model('tbc_paciente',pacientesShema);
module.exports=Pacientes;