var mongoose =require('mongoose');
var schema = mongoose.Schema;
var medicosShema = schema({

    nombre: String,
    apellido: String,
    documento_identidad: Number,
    ciudad: String,
    direccion: String,
    telefono: Number,
    email: String,
    tarjeta_profesional: Number,
    especialidad: String
   
   });

const Medicos = mongoose.model('tbc_medico',medicosShema);
module.exports=Medicos;