const Pacientes = require("../models/pacientes")

function prueba(req, res){
 res.status(200).send({
    message:'verificando la ejecucion del controlador'
 })

}

function savePacientes(req, res){
   var myPaciente = new Pacientes(req.body);
   myPaciente.save((err,result)=>{
    res.status(200).send({message:result});

   });
   
   
  }




  function buscarPaciente(req, res){
   var idPaciente= req.params.id;
    Pacientes.findById(idPaciente).exec((err,result)=>{
       if(err){
           res.status(500).send({message:"error en el backend"});
       }else{
           if(!result){
               res.status(400).send({message:"verificar los datos ingresados"});
           }else{
               res.status(200).send({message:result});
           }
          
       }
})

   }

   function updatePaciente(req, res){
    var idPaciente= req.params.id;
    Paciente.findOneAndUpdate({_id:idPaciente},req.body,{new:true},function(err,Paciente){
        if(err)
           res.send(err)
        res.json(Paciente)  


  });

        
   }


   function deletePaciente(req, res){
    var idPaciente = req.params.id;
    Paciente.findByIdAndDelete(idPaciente,function(err,Paciente){
        if(err){
            return res.json(500,{
              message: "no se encontro la cita a eliminar"
            })  
          }
  
          return res.json(Paciente);
  
      })
   }




  // function updatePacientes(req, res){
  //  res.status(200).send({
   //    message:'verificando la ejecucion del controlador'
   // })
   
  // }


module.exports={
    prueba,
    buscarPaciente,
    savePacientes,
    updatePaciente,
    deletePaciente
   
};
