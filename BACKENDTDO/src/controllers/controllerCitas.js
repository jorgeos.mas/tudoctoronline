const { updateLocale } = require("moment/moment");
const Citas = require("../models/citas")


function saveCitas(req, res){
   var myCita = new Citas(req.body);
    myCita.save((err,result)=>{
    res.status(200).send({message:result});
   
       });
}



  function buscarCita(req, res){
    var idCita = req.params.id;
    Citas.findById(idCita).exec((err,result)=>{
        if(err){
            res.status(500).send({message:"error en el backend"});
        }else{
            if(!result){
                res.status(400).send({message:"verificar los datos ingresados"});
            }else{
                res.status(200).send({message:result});
            }
           
        }
     })
}


   function updateCita(req, res){
    var idCita = req.params.id;
    Citas.findOneAndUpdate({_id:idCita},req.body,{new:true},function(err,Citas){
        if(err)
           res.send(err)
        res.json(Citas)  


  });

        
   }


   function deleteCita(req, res){
    var idCita = req.params.id;
    Citas.findByIdAndDelete(idCita,function(err,Citas){
        if(err){
            return res.json(500,{
              message: "no se encontro la cita a eliminar"
            })  
          }
  
          return res.json(Citas);
  
      })
   }




module.exports={
    
    saveCitas,
    buscarCita,
    updateCita,
    deleteCita
  
};
