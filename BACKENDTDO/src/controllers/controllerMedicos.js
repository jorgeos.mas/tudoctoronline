const Medicos = require("../models/medicos")


function saveMedicos(req, res){
   var myMedico = new Medicos(req.body);
   myMedico.save((err,result)=>{
    res.status(200).send({message:result});

   });
    
   
  }

  function buscarMedico(req, res){
   var idMedico= req.params.id;
   Medicos.findById(idMedico).exec((err,result)=>{
       if(err){
           res.status(500).send({message:"error en el backend"});
       }else{
           if(!result){
               res.status(400).send({message:"verificar los datos ingresados"});
           }else{
               res.status(200).send({message:result});
           }
          
       }

   })

}



function updateMedico(req, res){
    var idMedico= req.params.id;
    Medico.findOneAndUpdate({_id:idMedico},req.body,{new:true},function(err,Medico){
        if(err)
           res.send(err)
        res.json(Medico)  


  });

        
   }


   function deleteMedico(req, res){
    var idMedico = req.params.id;
    Medico.findByIdAndDelete(idMedico,function(err,Medico){
        if(err){
            return res.json(500,{
              message: "no se encontro la cita a eliminar"
            })  
          }
  
          return res.json(Medico);
  
      })
   }




module.exports={
    
    saveMedicos,
    buscarMedico,
    updateMedico,
    deleteMedico
    

};
