var express = require("express");
var app = express();

var bodyParser = require("body-parser");
var mongoose = require("mongoose");

app.use(express.json());
app.use(express.urlencoded({
    extended:true
}));

app.use((req, res, next)=>{
    res.header('Access-Control-Allow-origin','*');
    res.header('Access-Control-Allow-origin','Autorizacion, X-APY-KEY,Origin, XRequested-With,Content-Type,Accept,Access-Control-Allow-Request-Method');
    
    res.header('Control-Allow-Request-Method','GET,POST,PUT,DELETE,OPTTION'); 

    res.header('Allow','GET,POST,PUT,DELETE,OPTTION');

    next();

});

//llamar routers
app.use(require('./routers/routers'));


module.exports = app;
